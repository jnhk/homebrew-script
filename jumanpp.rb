class Jumanpp < Formula
  desc "Japanese Morphological Analyzer based on RNNLM"
  homepage "http://nlp.ist.i.kyoto-u.ac.jp/EN/index.php?JUMAN%2B%2B"
  url "https://github.com/ku-nlp/jumanpp/releases/download/v2.0.0-rc2/jumanpp-2.0.0-rc2.tar.xz"
  sha256 "179bb7d033a41fb451f30fdaf916ace1dd71253caf82aca1f32ad5e240abdf9d"

  depends_on "cmake"
  depends_on "boost"
  depends_on "boost-build"
  depends_on "gperftools"

  def install
    system "mkdir", "bld"
    system "cd", "bld"
    system "cmake", "-DCMAKE_BUILD_TYPE=Release",
                    "-DCMAKE_INSTALL_PREFIX=#{prefix}"
    system "make", "install"
  end

  test do
    ENV["LANG"] = "C.UTF-8" # prevent "invalid byte sequence in UTF-8" on sierra build
    system bin/"jumanpp", "--version"

    output = <<-EOI.undent
      こんにち こんにち こんにち 名詞 6 時相名詞 10 * 0 * 0 "代表表記:今日/こんにち カテゴリ:時間"
      は は は 助詞 9 副助詞 2 * 0 * 0 NIL
      EOS
      EOI

    assert_match output, pipe_output(bin/"jumanpp", "echo こんにちは")
  end
end
