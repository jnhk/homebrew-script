# homebrew-script

A repository of homebrew formula.

## Usage

`brew install --build-from-source ./<filename>.rb`

## Scripts

- [JUMAN++](http://nlp.ist.i.kyoto-u.ac.jp/index.php?JUMAN++) ([latest](https://github.com/ku-nlp/jumanpp/releases/), v2.0.0-rc2): Japanese Morphological Analyzer based on RNNLM.
- [KNP](http://nlp.ist.i.kyoto-u.ac.jp/index.php?KNP) ([latest](http://nlp.ist.i.kyoto-u.ac.jp/index.php?KNP#tae6acce), v4.19): Japanese dependency parser.
